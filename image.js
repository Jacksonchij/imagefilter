
var image;
var fileinput;
var canvas;
var imageLoaded = false;

function upload(){
    fileinput = document.getElementById("finput");
    canvas = document.getElementById("canvas");
    image = new SimpleImage(fileinput);
    imageLoaded = true;
    image.drawTo(canvas);
}

function makeGrey() {
    if(imageLoaded){
        for (var pixel of image.values()) {
            var rgb = (pixel.getRed() + pixel.getBlue() + pixel.getGreen()) / 3;
            pixel.setRed(rgb);
            pixel.setBlue(rgb);
            pixel.setGreen(rgb);
        }
        image.drawTo(canvas);
    }else{
        alert("No Image Found!")
    }
}

function makeRed() {
    if(imageLoaded){
        for (var pixel of image.values()) {
            var avg = (pixel.getRed() + pixel.getGreen() + pixel.getBlue()) / 3;
            if (avg < 128) {
                pixel.setRed(2 * avg);
                pixel.setGreen(0);
                pixel.setBlue(0);
            } else {
                pixel.setRed(255);
                pixel.setGreen(2 * avg - 255);
                pixel.setBlue(2 * avg - 255);
            }
        }
        image.drawTo(canvas);
    }else{
        alert("No Image Found!")
    }
}

function makeRainbow() {
    if(imageLoaded){
        var height = image.getHeight();
        for (var pixel of image.values()) {
            var y = pixel.getY();
            var avg = (pixel.getRed() + pixel.getGreen() + pixel.getBlue()) / 3;
            if (y < height / 7) {
                //red
                if (avg < 128) {
                    pixel.setRed(2 * avg);
                    pixel.setGreen(0);
                    pixel.setBlue(0);
                } else {
                    pixel.setRed(255);
                    pixel.setGreen(2 * avg - 255);
                    pixel.setBlue(2 * avg - 255);
                }
            } else if (y < height * 2 / 7) {
                //orange
                if (avg < 128) {
                    pixel.setRed(2 * avg);
                    pixel.setGreen(0.8*avg);
                    pixel.setBlue(0);
                } else {
                    pixel.setRed(255);
                    pixel.setGreen(1.2*avg-51);
                    pixel.setBlue(2 * avg - 255);
                }
            } else if (y < height * 3 / 7) {
                //yellow
                if (avg < 128) {
                    pixel.setRed(2 * avg);
                    pixel.setGreen(2*avg);
                    pixel.setBlue(0);
                } else {
                    pixel.setRed(255);
                    pixel.setGreen(255);
                    pixel.setBlue(2 * avg - 255);
                }
            } else if (y < height * 4 / 7) {
                //green
                if (avg < 128) {
                    pixel.setRed(0);
                    pixel.setGreen(2*avg);
                    pixel.setBlue(0);
                } else {
                    pixel.setRed(2*avg-255);
                    pixel.setGreen(255);
                    pixel.setBlue(2 * avg - 255);
                }
            } else if (y < height * 5 / 7) {
                //blue
                if (avg < 128) {
                    pixel.setRed(0);
                    pixel.setGreen(0);
                    pixel.setBlue(2*avg);
                } else {
                    pixel.setRed(2*avg-255);
                    pixel.setGreen(2 * avg - 255);
                    pixel.setBlue(255);
                }
            } else if (y < height * 6 / 7) {
                //indigo
                if (avg < 128) {
                    pixel.setRed(.8*avg);
                    pixel.setGreen(0);
                    pixel.setBlue(2*avg);
                } else {
                    pixel.setRed(1.2*avg-51);
                    pixel.setGreen(2 * avg - 255);
                    pixel.setBlue(255);
                }
            } else {
                //violet
                if (avg < 128) {
                    pixel.setRed(1.6*avg);
                    pixel.setGreen(0);
                    pixel.setBlue(1.6*avg);
                } else {
                    pixel.setRed(0.4*avg+153);
                    pixel.setGreen(2 * avg - 255);
                    pixel.setBlue(0.4*avg+153);
                }
            }
        }
        image.drawTo(canvas);
    }else{
        alert("No Image Found!");
    }
}

function makeBlur() {
    if(imageLoaded){
        var output = new SimpleImage(image.getWidth(), image.getHeight());
        for (var pixel of image.values()) {
            var x = pixel.getX();
            var y = pixel.getY();
            if (Math.random() > 0.5) {
                var other = getPixelNearby(image, x, y, 10);
                output.setPixel(x, y, other);
            }
            else {
                output.setPixel(x, y, pixel);
            }
        }
        image = output;
        image.drawTo(canvas);
    }else{
        alert("No Image Found!");
    }
}

function getPixelNearby (image, x, y, diameter) {
    var dx = Math.random() * diameter - diameter / 2;
    var dy = Math.random() * diameter - diameter / 2;
    var nx = ensureInImage(x + dx, image.getWidth());
    var ny = ensureInImage(y + dy, image.getHeight());
    return image.getPixel(nx, ny);
}

function ensureInImage (coordinate, size) {
    // coordinate cannot be negative
    if (coordinate < 0) {
        return 0;
    }
    // coordinate must be in range [0 .. size-1]
    if (coordinate >= size) {
        return size - 1;
    }
    return coordinate;
}



function reset(){
    if(imageLoaded){
        image = new SimpleImage(fileinput);
        image.drawTo(canvas);
    }
}

function clear(){
    var ctx = canvas.getContext('2d');
    canvas.removeAllRanges();
}